#Global Variables
$hiddenpath = $env:APPDATA + "\Desktop_Changer\"
$UpdatedPath = $hiddenpath + "Updated.txt"
$desktopsfile = $hiddenpath + "Desktops.csv"

$desktops = Import-Csv $desktopsfile
foreach ($desktop in $desktops) {
    $output = $hiddenpath + $Desktop.Number
    $url = $Desktop.Url
    Start-BitsTransfer -Source $url -Destination $output
}

$DesktopNum = Get-Content -Path $hiddenpath"page.txt"
$DesktopNum = [System.Decimal]::Parse($DesktopNum)
$NextDesktop = $DesktopNum + 1
if ($NextDesktop -gt 5) {
    $NextDesktop = 1
}
Set-Content -Path $hiddenpath"page.txt" -Value $NextDesktop
reg add "HKEY_CURRENT_USER\Control Panel\Desktop" /v Wallpaper /t REG_SZ /d  $hiddenpath$DesktopNum".jpg" /f
Start-Sleep -s 15
rundll32.exe user32.dll, UpdatePerUserSystemParameters, 0, $false

Set-ItemProperty -path "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "DesktopChanger" -Value $hiddenpath'DesktopChanger.exe'